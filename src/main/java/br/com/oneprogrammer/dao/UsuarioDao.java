package br.com.oneprogrammer.dao;

import br.com.oneprogrammer.domain.TipoSexo;
import br.com.oneprogrammer.domain.Usuario;

import java.util.List;

public interface UsuarioDao {

    void salvar(Usuario usuario);

    void editar(Usuario usuario);

    void excluir(Long id);

    Usuario getId(Long id);

    List<Usuario> getTodos();

    List<Usuario> getBySexo(TipoSexo sexo);

    List<Usuario> getByNome(String nome);
}
