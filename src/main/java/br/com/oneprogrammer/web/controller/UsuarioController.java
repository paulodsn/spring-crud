package br.com.oneprogrammer.web.controller;

import br.com.oneprogrammer.dao.UsuarioDao;
import br.com.oneprogrammer.domain.TipoSexo;
import br.com.oneprogrammer.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "usuario")
public class UsuarioController {

    @Autowired
    private UsuarioDao dao;

    @ModelAttribute("sexos")
    public TipoSexo[] tipoSexos() {
        return TipoSexo.values();
    }

    @GetMapping("/nome")
    public ModelAndView listarPorNome(@RequestParam(value = "nome") String nome, ModelMap model){
        if(nome == null){
            return new ModelAndView("redirect:/usuario/todos");
        }

        model.addAttribute("usuarios", dao.getByNome(nome));
        model.addAttribute("conteudo", "/user/list");
        return new ModelAndView("/layout", model);
    }

    @GetMapping("/sexo")
    public ModelAndView listarPorSexo(@RequestParam(value = "tipoSexo") TipoSexo sexo, ModelMap model){
        if(sexo == null){
            return new ModelAndView("redirect:/usuario/todos");
        }

        model.addAttribute("usuarios", dao.getBySexo(sexo));
        model.addAttribute("conteudo", "/user/list");
        return new ModelAndView("/layout", model);
    }

    @GetMapping("/todos")
    public ModelAndView home(ModelMap model) {
        model.addAttribute("usuarios", dao.getTodos());
        model.addAttribute("conteudo", "/user/list");
        return new ModelAndView("/layout", model);
    }

    @GetMapping("/cadastro")
    public ModelAndView cadastro(@ModelAttribute("usuario") Usuario usuario, ModelMap model){
        return new ModelAndView("/layout", "conteudo", "/user/add");
    }

    @PostMapping("/save")
    public ModelAndView save(@Valid Usuario usuario, BindingResult result, RedirectAttributes attr){
        if(result.hasErrors()){
            return new ModelAndView("/layout", "conteudo", "/user/add");
        }
        dao.salvar(usuario);
        attr.addFlashAttribute("message", "Usuário salvo com sucesso");
        return new ModelAndView("redirect:/usuario/todos");
    }

    @GetMapping("/update/{id}")
    public ModelAndView preUpdate(@PathVariable("id") Long id, ModelMap model) {
        Usuario usuario = dao.getId(id);
        model.addAttribute("usuario", usuario);
        model.addAttribute("conteudo", "/user/add");
        return new ModelAndView("/layout", model);
    }

    @PostMapping("/update")
    public ModelAndView update(@Valid @ModelAttribute("usuario") Usuario usuario, BindingResult result, RedirectAttributes attr){
        if(result.hasErrors()){
            return new ModelAndView("/layout", "conteudo", "/user/add");
        }
        dao.editar(usuario);
        attr.addFlashAttribute("message", "Usuário alterado com sucesso.");
        return new ModelAndView("redirect:/usuario/todos");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, RedirectAttributes attr){
        dao.excluir(id);
        attr.addFlashAttribute("message","Usuário deletado com sucesso.");
        return "redirect:/usuario/todos";
    }
}
